<?php
function stm_theme_import_sliders( $layout ) {
	$slider_names = apply_filters( 'stm_theme_sliders', true );

	if ( class_exists( 'RevSlider' ) ) {
		$sliders_path = apply_filters( 'stm_theme_demos_path', true ) . $layout . '/sliders/';
		foreach ( $slider_names as $slider_name ) {
			$slider_path = $sliders_path . $slider_name . '.zip';
			if ( file_exists( $slider_path ) ) {
				$slider = new RevSlider();
				$slider->importSliderFromPost( true, true, $slider_path );
			}
		}
	}
}
