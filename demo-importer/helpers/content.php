<?php
function stm_theme_import_content( $layout, $builder ) {
	set_time_limit( 0 );

	if ( ! defined( 'WP_LOAD_IMPORTERS' ) ) {
		define( 'WP_LOAD_IMPORTERS', true );
	}

	require_once CORE_MODULES_PATH . '/demo-importer/wordpress-importer/class-stm-wp-import.php';

	$wp_import                    = new STM_WP_Import();
	$wp_import->layout            = $layout;
	$wp_import->builder           = $builder;
	$wp_import->fetch_attachments = true;

	if ( defined( 'STM_DEV_MODE' ) ) {
		$ready = apply_filters( 'stm_theme_demos_path', true ) . $layout . '/xml/demo.xml';
	} else {
		$ready = stm_importer_download_demo( $layout );
	}

	if ( is_wp_error( $ready ) ) {
		return $ready;
	}

	if ( $ready ) {
		// Delete Menu
		stm_delete_all_menu();

		ob_start();
		$wp_import->import( $ready );
		ob_end_clean();
	}

	return true;
}

function stm_importer_download_demo( $layout ) {
	if ( ! class_exists( 'Plugin_Upgrader', false ) ) {
		require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
	}

	$theme_name = strtolower( wp_get_theme()->get( 'Name' ) );

	$upgrader = new WP_Upgrader( new Automatic_Upgrader_Skin() );
	$result   = $upgrader->run(
		array(
			'package'                     => "downloads://{$theme_name}/demos/{$layout}.zip",
			'destination'                 => apply_filters( 'stm_get_temp_path', '' ),
			'clear_destination'           => false,
			'abort_if_destination_exists' => false,
			'clear_working'               => true,
		)
	);

	if ( false === $result ) {
		$result = new WP_Error( '', 'WP_Upgrader returned "false" when downloading demo ZIP.' );
	}

	if ( is_wp_error( $result ) ) {
		return $result;
	}

	return $result['destination'] . "{$layout}.xml";
}

function stm_delete_all_menu() {
	$taxonomy_name = 'nav_menu';
	$terms         = get_terms(
		array(
			'taxonomy'   => $taxonomy_name,
			'hide_empty' => false,
		)
	);
	foreach ( $terms as $term ) {
		wp_delete_term( $term->term_id, $taxonomy_name );
	}
}

if ( ! function_exists( 'stm_get_temp_path' ) ) {
	function stm_get_temp_path() {
		$upload_dir = wp_upload_dir();
		$dir        = $upload_dir['basedir'] . '/tmp-stm/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0777, true );
		}
		return $dir;
	}
	add_filter( 'stm_get_temp_path', 'stm_get_temp_path' );
}
