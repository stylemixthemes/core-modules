<?php
/**
 * Apply Filter for Parsing Post Meta to Elementor Data (background images, attachment images urls Parser)
 */
add_filter( 'stm_wp_import_post_meta', 'core_modules_wp_import_post_meta_img_parser', 10, 1 );

function core_modules_wp_import_post_meta_img_parser( $post_meta ) {
	foreach ( $post_meta as $meta_index => $meta ) {
		if ( '_elementor_data' === $meta['key'] ) {
			core_modules_rebuilder_elementor_data( $post_meta[ $meta_index ]['value'] );
		}
	}
	return $post_meta;
}

function core_modules_rebuilder_elementor_data( &$data ) {
	if ( ! empty( $data ) ) {
		$data = maybe_unserialize( $data );
		if ( ! is_array( $data ) ) {
			$data = core_modules_is_elementor_data_unslash_required() ?  wp_unslash( $data ) : json_decode( $data, true );
		}
		core_modules_rebuilder_elementor_data_walk( $data, get_bloginfo( 'url' ) . '/' );
		$data = wp_slash( wp_json_encode( $data ) );
	}
}

function core_modules_rebuilder_elementor_data_walk( &$data_arg, $site_url ) {
	if ( is_array( $data_arg ) ) {
		foreach ( $data_arg as &$args ) {
			if ( ! empty( $args['url'] ) ) {
				$localhost   = 'https://business-theme.stylemixstage.com/';
				$args['url'] = str_replace( $localhost, $site_url, $args['url'] );
			}
			core_modules_rebuilder_elementor_data_walk( $args, $site_url );
		}
	}
}

function core_modules_is_elementor_data_unslash_required() {
	if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
		return false;
	}
	// before version 2.9.10 it was required
	if ( version_compare( ELEMENTOR_VERSION, '2.9.10', '<' ) ) {
		return true;
	}
	return false;
}
