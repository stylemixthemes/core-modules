<?php
function stm_set_content_options( $layout, $builder ) {
	/*Set menus*/
	$locations = get_theme_mod( 'nav_menu_locations' );
	$menus     = wp_get_nav_menus();

	if ( ! empty( $menus ) ) {
		foreach ( $menus as $menu ) {
			if ( is_object( $menu ) ) {
				$menu_names = apply_filters( 'stm_theme_menus', true );
				$menu_name  = $menu->name;
				if ( in_array( $menu_name, $menu_names, true ) ) {
					$locations['stm-header-menu'] = $menu->term_id;
				}
			}
		}
	}

	set_theme_mod( 'nav_menu_locations', $locations );

	//Set pages
	update_option( 'show_on_front', 'page' );

	$possible_home_pages = apply_filters( 'stm_theme_front_pages', true );

	foreach ( $possible_home_pages as $home_page ) {
		$front_page = get_page_by_title( $home_page );
		if ( isset( $front_page->ID ) ) {
			update_option( 'page_on_front', $front_page->ID );
		}
	}
}
