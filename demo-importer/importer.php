<?php
if ( is_admin() ) {
	add_action( 'wp_ajax_stm_demo_import_content', 'stm_demo_import_content' );
}

/**
 * Removed bookit redirect
 */
add_action(
	'admin_init',
	function () {
		delete_transient( 'fs_plugin_bookit_activated' );
	}
);

require_once CORE_MODULES_PATH . '/demo-importer/helpers/parsers.php';
require_once CORE_MODULES_PATH . '/demo-importer/helpers/content.php';
require_once CORE_MODULES_PATH . '/demo-importer/helpers/set-content.php';
require_once CORE_MODULES_PATH . '/demo-importer/helpers/slider.php';
require_once CORE_MODULES_PATH . '/demo-importer/helpers/theme_options.php';
require_once CORE_MODULES_PATH . '/demo-importer/helpers/widgets.php';

/**
 * Demo Import Ajax Action
 */
function stm_demo_import_content() {
	if ( ! current_user_can( 'administrator' ) ) {
		die;
	}
	check_ajax_referer( 'stm_demo_import_content', 'nonce' );
	$layout       = ! empty( $_GET['demo_template'] ) ? sanitize_title( $_GET['demo_template'] ) : 'default';
	$builder      = ! empty( $_GET['builder'] ) ? sanitize_title( $_GET['builder'] ) : 'js_composer';
	$import_data  = ! empty( $_GET['import_data'] ) ? sanitize_title( $_GET['import_data'] ) : '';
	$import_media = ! empty( $_GET['import_media'] ) ? ( 'true' === $_GET['import_media'] ) : false;

	// Run demo import parts
	$res = stm_demo_import_content_cli( $layout, $builder, $import_data, $import_media );
	if ( is_wp_error( $res ) ) {
		wp_send_json_error( $res, 400 );
	}

	if ( ! empty( $import_data ) ) {
		wp_send_json(
			array(
				'imported' => $import_data,
			)
		);
	} else {
		wp_send_json(
			array(
				'url'                 => get_bloginfo( 'url' ),
				'title'               => esc_html__( 'View site', 'core-modules' ),
				'theme_options_title' => esc_html__( 'Theme options', 'core-modules' ),
				'theme_options'       => esc_url_raw( admin_url( 'customize.php' ) ),
			)
		);
	}

	die();
}

/**
 * Run Demo Import
 *
 * @param $layout
 * @param $builder
 * @param $import_data
 * @param $import_media
 *
 * @return array|bool|string|\WP_Error
 */
function stm_demo_import_content_cli( $layout, $builder, $import_data, $import_media ) {
	switch ( $import_data ) {
		case 'content':
			/** Import content */
			do_action( 'before_importer_done', $layout );
			return stm_theme_import_content( $layout, $builder );
		case 'theme_options':
			/** Import theme options */
			stm_set_layout_options( $layout );
			break;
		case 'sliders':
			/** Import sliders */
			stm_theme_import_sliders( $layout );
			break;
		case 'widgets':
			/** Import Widgets */
			stm_theme_import_widgets( $layout );
			/** Set menu and pages */
			stm_set_content_options( $layout, $builder );
			break;
		default:
			do_action( 'after_importer_done', $layout );
			update_option( 'stm_demo_name', $layout );
	}
}
