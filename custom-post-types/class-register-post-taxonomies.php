<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Register_Post_Taxonomies {
	public function __construct() {
		add_action( 'init', array( $this, 'taxonomies_init' ) );
	}

	public function taxonomies_init() {
		$taxonomies = apply_filters( 'stm_post_taxonomies', true );

		if ( isset( $taxonomies ) ) {
			foreach ( $taxonomies as $taxonomy => $taxonomy_args ) {
				$add_args = ( ! empty( $taxonomy_args['args'] ) ) ? $taxonomy_args['args'] : array();
				$args     = $this->taxonomies_args(
					$this->taxonomies_labels(
						$taxonomy_args['single'],
						$taxonomy_args['plural']
					),
					$taxonomy,
					$add_args
				);
				register_taxonomy( $taxonomy, $taxonomy_args['post_type'], $args );
			}
		}
	}
	public function taxonomies_labels( $singular, $plural ) {
		//phpcs:disable
		return array(
			'name'              => _x( $plural, 'taxonomy general name', 'core-modules' ),
			'singular_name'     => _x( $singular, 'taxonomy general name', 'core-modules' ),
			'search_items'      => __( 'Search ' . $singular . ' category', 'core-modules' ),
			'all_items'         => __( 'All ' . $plural . ' category', 'core-modules' ),
			'parent_item'       => __( 'Parent ' . $singular . ' category', 'core-modules' ),
			'parent_item_colon' => __( 'Parent ' . $singular . ' category', 'core-modules' ),
			'edit_item'         => __( 'Edit ' . $plural . ' category', 'core-modules' ),
			'update_item'       => __( 'Update %s category ' . $singular, 'core-modules' ),
			'add_new_item'      => __( 'Add new ' . $singular . ' category', 'core-modules' ),
			'new_item_name'     => __( 'New ' . $singular . ' category name', 'core-modules' ),
			'menu_name'         => __( 'Categories', 'core-modules' ),
		);
		//phpcs:enable
	}

	public function taxonomies_args( $labels, $slug, $args = array() ) {
		$default_args = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
		);

		return wp_parse_args( $args, $default_args );
	}
}

new Register_Post_Taxonomies();
