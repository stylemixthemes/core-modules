<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Register_Post_Types {
	public function __construct() {
		add_action( 'init', array( $this, 'post_types_init' ) );
	}

	public function post_types_init() {
		$post_types = apply_filters( 'stm_post_types', true );

		if ( isset( $post_types ) ) {
			foreach ( $post_types as $post_type => $post_type_info ) {
				$add_args = ( ! empty( $post_type_info['args'] ) ) ? $post_type_info['args'] : array();
				$args     = $this->post_type_args(
					$this->post_types_labels(
						$post_type_info['single'],
						$post_type_info['plural']
					),
					$post_type,
					$add_args
				);

				register_post_type( $post_type, $args );
			}
		}
	}

	public function post_types_labels( $singular, $plural ) {
		$admin_bar_name = ( ! empty( $admin_bar_name ) ) ? $admin_bar_name : $plural;
		//phpcs:disable
		return array(
			'name'               => _x( $plural, 'post type general name', 'core-modules' ),
			'singular_name'      => _x( $singular, 'post type singular name', 'core-modules' ),
			'menu_name'          => __( $plural, 'core-modules' ),
			'name_admin_bar'     => _x( $admin_bar_name, 'Admin bar ' . $singular . ' name', 'starter-core' ),
			'add_new_item'       => __( 'Add New ' . $singular, 'core-modules' ),
			'new_item'           => __( 'New ' . $singular, 'core-modules' ),
			'edit_item'          => __( 'Edit ' . $singular, 'core-modules' ),
			'view_item'          => __( 'View ' . $singular, 'core-modules' ),
			'all_items'          => _x( $admin_bar_name, 'Admin bar ' . $singular . ' name', 'starter-core' ),
			'search_items'       => __( 'Search ' . $plural, 'core-modules' ),
			'parent_item_colon'  => __( 'Parent ' . $plural . ':', 'core-modules' ),
			'not_found'          => __( 'No ' . $plural . ' found.', 'core-modules' ),
			'not_found_in_trash' => __( 'No ' . $plural . ' found in Trash.', 'core-modules' ),
		);
		//phpcs:enable
	}

	public function post_type_args( $labels, $slug, $args = array() ) {
		$can_edit     = ( current_user_can( 'edit_posts' ) );
		$default_args = array(
			'labels'             => $labels,
			'public'             => $can_edit,
			'publicly_queryable' => $can_edit,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $slug ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' ),
		);

		return wp_parse_args( $args, $default_args );
	}
}

new Register_Post_Types();
