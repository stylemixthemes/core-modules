<?php
define( 'CORE_MODULES_VERSION', '1.0.1' );
define( 'CORE_MODULES_PATH', dirname( __FILE__ ) );
define( 'CORE_MODULES_FILES_PATH', plugins_url( '/', __FILE__ ) );

require_once CORE_MODULES_PATH . '/custom-post-types/class-register-post-taxonomies.php';
require_once CORE_MODULES_PATH . '/custom-post-types/class-register-post-types.php';
require_once CORE_MODULES_PATH . '/demo-importer/importer.php';
require_once CORE_MODULES_PATH . '/nuxy/NUXY.php';
